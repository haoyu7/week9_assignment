# Project 9 - Honeypot


> Objective: Setup a honeypot and intercept some attempted attacks in the wild.

## Setup:
For this assignemnt I am using technich from my own resource.
Reference URL: https://null-byte.wonderhowto.com/how-to/hack-like-pro-set-up-honeypot-avoid-them-0153391/

## Step 1
I am using Kfsensor to host the honey spot.

![week9_1](https://user-images.githubusercontent.com/25192309/38474061-5cc5b55a-3b67-11e8-9fd6-42d023742267.PNG)

## Step 2
And I ran the "nmap -sS" command on Kali with my Ip address to scan.

![week9_2](https://user-images.githubusercontent.com/25192309/38474176-4cf79452-3b69-11e8-88fb-467ad9bc0d70.PNG)

## Step 3
Then I use nikto to find the vulneralbility of the IP

![week9_3](https://user-images.githubusercontent.com/25192309/38474254-6b0e5074-3b6a-11e8-9cd4-c858a10dc121.PNG)

## Step 4
For this step, I am using banner Banner Grab
![week9](https://user-images.githubusercontent.com/25192309/38477372-3b6c0c92-3b80-11e8-944b-2914efb42894.gif)




